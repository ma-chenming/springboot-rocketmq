import com.xiaoma.SpringbootRocketmqProducerApplication;
import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest(classes = {SpringbootRocketmqProducerApplication.class})
class SpringbootRocketmqApplicationTests {
    @Autowired
    RocketMQTemplate rocketMQTemplate;

    @Test
    void produce() {
        rocketMQTemplate.convertAndSend("theme1","content1");
    }
}
